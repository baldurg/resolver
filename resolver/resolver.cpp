// resolver.cpp : Defines the entry point for the console application.
//

#ifndef _UNIX
#include "stdafx.h"
#endif
#include <stdio.h>
#include <stdarg.h>
#include <ctype.h>
#include <json.h>
#include <string>
#include <map>
#include <vector>
#ifndef _UNIX
#include <windows.h>
#include "Shlwapi.h"
#endif

std::vector<std::string> unresolved;
std::vector<std::string> warnings;
std::vector<std::string> infos;
bool dodoc = false;
bool release = false;

int get_size_from_type(int type) 
{
	int size = 1;
	switch(type) {
		case 1:
			break;
		case 2:
			size = 2;
			break;
		case 3:
		case 5:
		case 9:
		case 10:
			size = 4;
			break;
		case 4:
		case 6:
			size = 8;
	}
	return size;
}

inline json_object *jsooget(json_object *object, const char* key) {
  json_object *res;
  if(json_object_object_get_ex(object, key, &res)) return res;
  return NULL;
}

int grepsyms(const char* filename, const char* findvar, unsigned long *address, unsigned long *bytesize) {
	char line[256];
	int found = 0;
	FILE *fp;
#ifndef _UNIX
	fopen_s(&fp, filename, "r");
#else
	fp = fopen(filename, "r");
#endif
	if(!fp) {
		printf("Unable to open '%s'\n", filename);
		return 0;
	}
	while(fgets(line, sizeof(line)-1, fp)) {
		unsigned long addr;
		char varname[256];
		char vartype[256];
		char drasl[256];
		int bitsize = 0;
		int bitpos = 0;
//		if(!strstr(line, ".bss")) continue;
		//printf("Read '%s'\n", line);
		if(sscanf(line, "%s %s %s %s %d %s%s %d", vartype, varname, drasl, drasl, &bitsize, drasl, drasl, &bitpos) == 8) {
			for(int i = 0; i < strlen(varname); i++) {
				if(varname[i] == ';' || varname[i] == '[') varname[i] = '\0';
			}
			//printf("Output '%s' '%s' '%s' %u %u\n", vartype, varname, drasl, bitsize, bitpos);
			//printf("Output '%x': '%s'\n", addr, varname);
			if(!strcmp(varname, findvar)) {
				//printf("It's the one I was looking for\n");
				*address = bitpos / 8;
				*bytesize = bitsize / 8;
				found++;
			}
		}
		
	}
	fclose(fp);
	return found;
}

int grepsize(const char* filename) {
	char line[256];
	FILE *fp;
	int size = 0;
#ifndef _UNIX
	fopen_s(&fp, filename, "r");
#else
	fp = fopen(filename, "r");
#endif
	if(!fp) {
		printf("Unable to open '%s'\n", filename);
		return 0;
	}
	while(fgets(line, sizeof(line)-1, fp)) {
		char varname[256];
		if(sscanf(line, "struct %s { /* size %d id", varname, &size) == 2 && varname[0] != '_') {
			printf("Struct name is '%s'\n", varname);
			break;
		}
	}
	fclose(fp);
	return size;
}

int translateworker(json_object *array, char* deffilename)
{
	char tmpbuf[256];
	char warnstr[256];
	int count = 0;
		for(int i = 0; i < json_object_array_length(array); i++) {
		unsigned long address;
		unsigned long bytesize;
		const char* addrstr;
		json_object *item = json_object_array_get_idx(array, i);
		const char* itemid = json_object_get_string(jsooget(item, "id"));
		sprintf(tmpbuf, "%s", itemid);
		const int type = json_object_get_int(jsooget(item, "type"));
		int compact = json_object_get_int(jsooget(item, "compact"));
		int arraysize = json_object_get_int(jsooget(item, "array"));
		int devonly = json_object_get_int(jsooget(item, "devonly"));
		if(devonly && release) {
			printf("Ignoring item '%s' in release mode\n", itemid);
			json_object_object_add(item, "ignore", json_object_new_int(1));
			continue;
		}
		int countoffs = 0;
		int arrayoffs = -1;
		if(compact) { 
			tmpbuf[strlen(itemid)-1] = '\0';
			countoffs = itemid[strlen(itemid)-1];
			if(countoffs >= 0x30 && countoffs <= 0x39) {
				countoffs -= 0x30;
			} else {
				countoffs = 0;
			}
		} else {
			char *period = strstr(tmpbuf, ".");
			if(period) {
				period[0] = '\0';
				arrayoffs = atoi(&period[1]);
			}
		}
		if(grepsyms(deffilename, tmpbuf, &address, &bytesize)) {
			int typesize = get_size_from_type(type);
			if(type == 7) {
				typesize = json_object_get_int(jsooget(item, "width"));
				if(!typesize) typesize = 1;
			}
			if(arrayoffs >= 0) {
				address += arrayoffs * typesize;
			}
			printf("address of '%s' found at %lx\n", itemid, address);
			json_object_object_add(item, "address", json_object_new_int(address));
			if(compact) {
				bool describedincr = false;
				printf("%s is in fact %d objects\n", itemid, compact);				
				int offset = 0;
				if (arraysize) typesize *= arraysize;
				int defsize = typesize * compact;
				json_object_object_add(item, "compact", json_object_new_int(0));
				const char* describedby = json_object_get_string(jsooget(item, "describedby"));
				if(describedby && describedby[strlen(describedby)-1] == '+') describedincr = true;
				char* describedbasename = NULL;
				int describedbaseoffs = 0;
				if(describedincr) {
					describedbasename = strdup(describedby);
					describedbasename[strlen(describedbasename)-1] = '\0';
					json_object_object_add(item, "describedby", json_object_new_string(describedbasename));
					describedbaseoffs = describedbasename[strlen(describedbasename)-1] - 0x30;
					describedbasename[strlen(describedbasename)-1] = '\0';
				}
				json_object *rows = jsooget(item, "rows");
				json_object *cols = jsooget(item, "cols");
				bool rowsinc = false;
				char *rowsbase = NULL;
				int rowsoffset = 0;
				if (rows && json_object_is_type(rows, json_type_string)) {
					const char *namedrows = json_object_get_string(rows);
					if (namedrows[strlen(namedrows) - 1] == '+') {
						rowsinc = true;
						rowsbase = strdup(namedrows);
						rowsbase[strlen(rowsbase) - 1] = '\0';
						json_object_object_add(item, "rows", json_object_new_string(rowsbase));
						rowsoffset = rowsbase[strlen(rowsbase) - 1] - 0x30;
						rowsbase[strlen(rowsbase) - 1] = '\0';
					}
				}
				bool colsinc = false;
				char *colsbase = NULL;
				int colsoffset = 0;
				if (cols && json_object_is_type(cols, json_type_string)) {
					const char *namedcols = json_object_get_string(cols);
					if (namedcols[strlen(namedcols) - 1] == '+') {
						colsinc = true;
						colsbase = strdup(namedcols);
						colsbase[strlen(colsbase) - 1] = '\0';
						json_object_object_add(item, "cols", json_object_new_string(colsbase));
						colsoffset = colsbase[strlen(colsbase) - 1] - 0x30;
						colsbase[strlen(colsbase) - 1] = '\0';
					}
				}
				if(defsize != bytesize) {
					char warnstr[256];
					sprintf(warnstr, "WARNING: Item %s is %lu bytes in sym but def accounts for %d bytes\n", itemid, bytesize, defsize);
					printf("%s", warnstr);
					warnings.push_back(warnstr);
				}
				for(int j = 1; j < compact; j++) {
					offset++;
					sprintf(tmpbuf, "%s", itemid);
					sprintf(tmpbuf+strlen(itemid)-1, "%d", countoffs + offset);
					json_object *newitem = json_tokener_parse(json_object_to_json_string(item)) ;
					json_object_object_add(newitem, "id", json_object_new_string(tmpbuf));
					json_object_object_add(newitem, "address", json_object_new_int(address + offset * typesize));
					json_object_object_add(newitem, "compact", json_object_new_int(0));
					if(describedincr) {
						sprintf(tmpbuf, "%s%d", describedbasename, describedbaseoffs + offset);
						json_object_object_add(newitem, "describedby", json_object_new_string(tmpbuf));
					}
					if (rowsinc) {
						sprintf(tmpbuf, "%s%d", rowsbase, rowsoffset + offset);
						json_object_object_add(newitem, "rows", json_object_new_string(tmpbuf));
					}
					if (colsinc) {
						sprintf(tmpbuf, "%s%d", colsbase, colsoffset + offset);
						json_object_object_add(newitem, "cols", json_object_new_string(tmpbuf));
					}
					json_object_array_add(array, newitem);
				}
				if(describedincr) free(describedbasename);
			} else {
				if(arrayoffs >= 0) {
					if((arrayoffs * typesize) > bytesize) {
						sprintf(warnstr, "WARNING: Item %s is past end of sym\n", itemid);
						printf("%s", warnstr);
						warnings.push_back(warnstr);
					}
				} else {
					int defsize = arraysize ? arraysize * typesize : typesize;
					if(defsize != bytesize) {
						sprintf(warnstr, "WARNING: Item %s is %lu bytes in sym but def accounts for %d bytes\n", itemid, bytesize, defsize);
						printf("%s", warnstr);
						warnings.push_back(warnstr);
					}
				}
			}
			count++;
		} else if((addrstr = json_object_get_string(jsooget(item, "address")))) {
			char *addrtmp = strdup(addrstr);
			char *openbracket = strstr(addrtmp, "[");
			char *period = strstr(addrtmp, ".");
			if(openbracket) {
				openbracket[0] = '\0';
				char *closebracket = strstr(addrtmp, "]");
				if(closebracket) closebracket[0] = '\0';
				int offset = atoi(&openbracket[1]);
				printf("Furiously trying '%s->%d' for %s\n", addrtmp, offset, itemid);
				int itemtype = json_object_get_int(jsooget(item, "type"));
				switch(itemtype) {
					case 2:
						offset = offset * 2;
						break;
					case 3:
					case 5:
					case 9:
						offset = offset * 4;
						break;
					case 4:
					case 6:
						offset = offset * 8;
					default:
						break;
				}
				if(grepsyms(deffilename, addrtmp, &address, &bytesize)) {
					printf("Found %s at %lx\n", itemid, address + offset);
					count++;
					json_object_object_add(item, "address", json_object_new_int(address + offset));
				}
				
			} else if(compact && period) {
				printf("%s is in fact %d virtual objects\n", itemid, compact);
				period[0] = '\0';
				int lowerbit = 0;
				int upperbit = 0;
				char* nextper = strstr(period+1, ".");
				if(nextper) {
					nextper[0] = '\0';
					upperbit = atoi(nextper+1);
				}
				lowerbit = atoi(period+1);
				int bitsize = upperbit - lowerbit + 1;
				char* parentobject = addrtmp;
				unsigned char lastc = parentobject[strlen(parentobject)-1];
				int parentoffset = -1;
				if(lastc >= 0x30 && lastc <= 0x39) {
					parentoffset = lastc - 0x30;
					parentobject[strlen(parentobject)-1] = '\0';
				}
				for(int j = 1; j < compact; j++) {
					sprintf(tmpbuf, "%s", itemid);
					sprintf(tmpbuf+strlen(itemid)-1, "%d", countoffs + j);
					json_object *newitem = json_tokener_parse(json_object_to_json_string(item)) ;
					json_object_object_add(newitem, "id", json_object_new_string(tmpbuf));
					if(type == 0 && parentoffset == -1) {
						sprintf(tmpbuf, "%s.%d", parentobject, j + lowerbit);
					} else if(type == 8 && parentoffset == -1) {
						sprintf(tmpbuf, "%s.%d.%d", parentobject, j*bitsize + lowerbit, j*bitsize + upperbit);
					} else if(type == 8) {
						sprintf(tmpbuf, "%s%d.%d.%d", parentobject, j + parentoffset, lowerbit, upperbit);
					} else {
						sprintf(tmpbuf, "%s%d.%d", parentobject, j + parentoffset, lowerbit);
					}
					json_object_object_add(newitem, "address", json_object_new_string(tmpbuf));
					json_object_object_add(newitem, "compact", json_object_new_int(0));
					json_object_array_add(array, newitem);
				}
			}
			free(addrtmp);
		} else {
			sprintf(warnstr, "%s unresolved\n", itemid);
			printf("%s", warnstr);
			unresolved.push_back(warnstr);
		}
	}
	return count;
}

int translate(char* deffilename, char* outputfilename, char* rtdefname, int npages, char* pagedefs[], char* docdir)
{
	int count = 0;
	char tmpstr[256];
	json_object *deffile = json_object_from_file(deffilename);
	if(!deffile) { printf("Unable to open json file\n"); return 1; }
	json_object *definition = jsooget(deffile, "definition");
	json_object *confdef = jsooget(definition, "confdef");
	printf("Translating %d pages\n", npages);
	for(int idx = 0; idx < npages && idx < json_object_array_length(confdef); idx++) {
		json_object *confpage = json_object_array_get_idx(confdef, idx);
		int page = json_object_get_int(jsooget(confpage, "page"));
		printf("Translating page %d from file %s\n", page, pagedefs[idx]);
		json_object *pagedata = jsooget(confpage, "data");
		int pagesize = json_object_get_int(jsooget(confpage, "size"));
		int structsize = grepsize(pagedefs[idx]);
		if((structsize > pagesize) || (structsize < 1)) {
			printf("ERROR: page %d struct invalid size, is %d but allowance is %d\n", page, structsize, pagesize);
			return 1;
		} else {
			sprintf(tmpstr,"Struct for page %d uses %d out of %d available bytes\n", page, structsize, pagesize);
			printf("%s", tmpstr);
			infos.push_back(tmpstr);
		}
		count += translateworker(pagedata, pagedefs[idx]);
	}
	json_object *rtdef = jsooget(definition, "datastream");
	json_object *rtvars = jsooget(rtdef, "variables");
	int rtstructsize = grepsize(rtdefname);
	int rtsize = json_object_get_int(jsooget(rtdef, "streamlength"));
	sprintf(tmpstr, "Real time struct uses %d out of %d available bytes\n", rtstructsize, rtsize);
	printf("%s", tmpstr);
	infos.push_back(tmpstr);
	translateworker(rtvars, rtdefname);
#if 1
	if(dodoc) {
		char* readbuf = (char*)malloc(262144);
		HANDLE hFind;
		WIN32_FIND_DATA data;
		char path[256];
		sprintf(path, "%s\\*.txt", docdir);
		hFind = FindFirstFile(path, &data);
		if (hFind != INVALID_HANDLE_VALUE) {
			json_object *doc = json_object_new_object();
			do {
				DWORD bytes;
				sprintf(path, "%s\\%s", docdir, data.cFileName);
				if(data.cFileName[strlen(data.cFileName)-4] == '.' && ((data.cFileName[strlen(data.cFileName)-1] | 0x20 )== 't') ) {
					HANDLE filehdl = CreateFile(path, GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_DELETE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
					strcpy(path, data.cFileName);
					printf("%s\n", path);
					PathRemoveExtension(path);
					if(filehdl) {
						BOOL succ = ReadFile(filehdl,readbuf,262144,&bytes,NULL);
						if(succ) {
							readbuf[bytes] = '\0';
							json_object_object_add(doc, path, json_object_new_string(readbuf));
						} else {
							printf("Failed to include documentation '%s'\n", path);
						}
					}
				}
			} while (FindNextFile(hFind, &data));
			FindClose(hFind);
			json_object_object_add(definition, "documentation", doc);
		}
	}
#endif

	if(!release) {
		const char *sig = json_object_get_string(json_object_object_get(definition, "signature"));
		if(sig) {
			char *newsig = (char*)malloc(strlen(sig)+5);
			printf("Appending dev flag to signature\n");
			sprintf(newsig, "%s-dev", sig);
			json_object_object_add(definition, "signature", json_object_new_string(newsig));
			free(newsig);
		}
	}

	for (std::vector<std::string>::iterator it = infos.begin(); it != infos.end(); it++) {
		printf("%s", it->c_str());
	}

	count += json_object_to_file_ext(outputfilename, deffile, JSON_C_TO_STRING_PRETTY);
	if(unresolved.size()) { 
		printf("%d items unresolved\n", unresolved.size()); 
		for (std::vector<std::string>::iterator it = unresolved.begin(); it != unresolved.end(); it++) {
			printf("%s", it->c_str());
		}
	}
	if(warnings.size()) { 
		printf("%d warnings\n", warnings.size()); 
		for (std::vector<std::string>::iterator it = warnings.begin(); it != warnings.end(); it++) {
			printf("%s", it->c_str());
		}
	}
	return 0;
}

#ifndef _UNIX
int _tmain(int argc, _TCHAR* argv[])
#else
int main(int argc, char* argv[])
#endif
{
	printf("argc is %d\n", argc);
	if(argc < 5) { printf("Invalid argument\nUsage: resolver.exe [flags] input.json output.json realtime.sym page0.sym page1.sym....\n"); return 1; }
	int argo = 1;
	while(argv[argo][0] == '-') {
		if(!strcmp(argv[argo] + 1, "release")) release = true;
		argo++;
	}
	if(strstr(argv[argc-1], ".") == NULL) dodoc = true;
	return translate(argv[argo], argv[argo+1], argv[argo+2], argc - (dodoc ? 4 : 3) - argo , &argv[argo+3], argv[argc-1]);
}

